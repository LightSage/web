---
layout: home
title: Home
permalink: /
---
<center>
  <h3>Hi!</h3>
  <p>I do cool stuff.<br><br>My current projects are:
  <ul>
    <li><a href="https://lightning.lightsage.dev">Lightning</a>, a Discord bot featuring customizable moderation and some other stuff.</li>
    <li><a href="https://udb-api.lightsage.dev">UDB-API</a>, an actual API for Universal-DB.</li>
  </ul>
  You can find me on <a href="https://gitlab.com/LightSage">GitLab</a>, <a href="https://github.com/LightSage">GitHub</a>.
  <br>
  Like one of my projects? Leave a donation at <a href="https://ko-fi.com/lightsage">Ko-Fi</a>
  </p>
